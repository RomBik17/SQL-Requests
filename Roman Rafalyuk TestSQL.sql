-- 1, 2
-- ���� ����� ����� ������������: � ������� DMS ������� SORT ����� ����� ���'���,
--								������ �� ��� ����� ����� �������� ����� ��'���� ������� DMS � TOV ����� ���� KTOV 
--							   ����� �� ������� � ������� DMS ������� NDM ��� ������ � �������� DMZ, 
--								�� ������� �� ����������� ���������.(���� � ����� �� ������� �� ������� DMS, ��� �������� ��������)



--3
USE [Test_Data]
GO

-- 3.1
SELECT [TOV].[NTOV], SUM([DMS].[KOL]) AS [sum_count], SUM([DMS].[CENA] * [DMS].[KOL]) AS [sum_cena]
FROM ([dbo].[DMS] INNER Join [dbo].[TOV] ON [DMS].[KTOV] = [TOV].[KTOV])
	INNER JOIN [dbo].[DMZ] ON [DMS].[NDM] = [DMZ].[NDM]
WHERE [DMZ].[PR] = 2 AND [DMZ].[DDM] = '2014-05-01'
GROUP BY [DMS].[KTOV], [TOV].[NTOV]
ORDER BY [sum_cena] DESC
GO

-- �3.2
UPDATE [DMS] SET [SORT] = [TOV].[SORT]
   FROM [TOV]
   WHERE [DMS].[KTOV] = [TOV].[KTOV]
GO

--3.3
SELECT t.[NTOV], SUM(IIF(d.[PR] = 2, - s.[KOL], s.[KOL])) AS [Ostatok],
    SUM(IIF(d.[PR] = 2, s.[CENA] * s.[Kol], - s.[CENA] * s.[Kol])) AS [Suma ostatka]
from [TOV] t JOIN [DMS] s on s.[KTOV] = t.[KTOV] JOIN [DMZ] d on d.[NDM] = s.[NDM]
group by t.[KTOV], t.[NTOV]
ORDER BY t.[NTOV]
GO

-- 3.4
INSERT INTO [dbo].[DMZ]
           ([DDM]
           ,[NDM]
           ,[PR])
     VALUES
           ( GETDATE()
           , (SELECT IIF(COUNT([NDM]) = 0, 1, MAX([NDM]) + 1)
		   FROM [DMZ])
           , (IIF((SELECT COUNT([NDM]) FROM DMZ) = 0, 1,
		   ( IIF ((SELECT TOP 1 [PR]
		   FROM [DMZ]
		   GROUP BY [PR]
		   ORDER BY COUNT([PR]) DESC) = 1, 2, 1)
		   ))))
GO

-- 3.5
INSERT INTO [DMS]
SELECT
    [KTOV],
    [KOL],
    [CENA],
    [SORT],
    (SELECT MIN([DMZ].[NDM]) FROM [DMZ]) [NDM]    
FROM [DMS] first
WHERE first.[NDM] = (SELECT MAX(second.[NDM]) FROM [DMZ] second)
AND first.[KTOV] NOT IN (
    SELECT second.[KTOV]
    FROM [DMS] second
    WHERE second.NDM = (SELECT MIN(second.[NDM]) FROM [DMZ] second)
)
